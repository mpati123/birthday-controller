package com.patneh.simpleWebApp;

import com.patneh.simpleWebApp.controller.BirthdayController;
import com.patneh.simpleWebApp.type.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

public class BirthdayServlet extends HttpServlet {
    BirthdayController birthdayController = new BirthdayController();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = new User(req.getParameter("name"), req.getParameter("surname"), LocalDate.parse(req.getParameter("birthDate")));
        resp.getWriter().write(birthdayController.isBirthday(user));
    }



}

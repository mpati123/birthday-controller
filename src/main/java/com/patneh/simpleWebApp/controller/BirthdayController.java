package com.patneh.simpleWebApp.controller;

import com.patneh.simpleWebApp.type.User;

import java.time.LocalDate;

public class BirthdayController {
    public String isBirthday(User user){
        int toBirthday = howLongToBirthday(user);
        String TO_BIRTHDAY_LEFT = "Do urodzin tego użytkownika zostało: ";
        String BIRTHDAY_DAY = "Gratulacje " +
                user.getName() + " " +
                user.getLastname() + ", dzisiaj kończysz " +
                user.getAge() + " lata. Wszystkiego najlepszego!";
        if(toBirthday == 0){
            return BIRTHDAY_DAY;
        }
        return TO_BIRTHDAY_LEFT + toBirthday + " dni!";
    }


    public int howLongToBirthday(User user){
        final int leapYear = 366;
        final int normalYear = 365;
        int today = LocalDate.now().getDayOfYear();
        int birthday = user.getBirthDate().getDayOfYear();
        if(today > birthday){
            if(LocalDate.now().isLeapYear()){
                return leapYear - today + birthday;
            }
            return normalYear - today + birthday;
        }
        return birthday - today;
    }
}

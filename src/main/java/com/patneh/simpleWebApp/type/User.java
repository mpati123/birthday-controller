package com.patneh.simpleWebApp.type;

import java.time.LocalDate;

public class User {
    private String name;
    private String lastname;
    private int age;
    private LocalDate birthDate;

    public User(String name, String lastname, LocalDate birthDate) {
        this.name = name;
        this.lastname = lastname;
        this.birthDate = birthDate;
        this.age = age();
    }

    private int age(){
        int todayYear = LocalDate.now().getYear();
        int birthYear = birthDate.getYear();
        return todayYear - birthYear;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public LocalDate getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
}
